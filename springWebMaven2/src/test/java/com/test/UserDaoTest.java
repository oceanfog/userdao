package com.test;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dao.UserDao;

import domain.User;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/applicationContext.xml")
public class UserDaoTest {

	private UserDao dao;
	private User user1, user2, user3;
	
	
	@Autowired
	private ApplicationContext context;
	
	@Before
	public void setUp(){
		this.dao = context.getBean("userDao", UserDao.class);
		this.user1 = new User("krkim", "1123", "kyeongrok");
		this.user2 = new User("yhkim", "1123", "yeonghwan");
	}
	
	@Test
	public void addAndGet() throws ClassNotFoundException, SQLException{
		
		
		dao.deleteAll();
		assertEquals( dao.getCount(), 0 ) ;
		
		dao.add(user1);
		assertEquals( dao.getCount(), 1 ) ;
		
		dao.add(user2);
		assertEquals( dao.getCount(), 2 ) ;
		
		String id1 = user1.getId();
		String id2 = dao.get( id1 ).getId();
		
		assertEquals(id1, id2);
		
		
	}
	
	
	@Test(expected=EmptyResultDataAccessException.class)
	
	public void getFailure() throws SQLException, ClassNotFoundException{
		
		dao.deleteAll();
		assertEquals( dao.getCount(), 0 );
		
		dao.get("unknown_id");
		
	}
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JUnitCore.main("com.test.UserDaoTest");

	}

}
