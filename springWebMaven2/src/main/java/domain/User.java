package domain;

public class User {

	String id;
	String password;
	String name;
	
	public User(String p_id, String p_password, String p_name){
		
		this.id = p_id;
		this.password = p_password;
		this.name = p_name;
	}
	
	public User(){
		
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
