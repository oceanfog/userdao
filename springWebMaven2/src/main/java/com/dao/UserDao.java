package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import domain.User;

public class UserDao {
	
	private DataSource dataSource;
	
	public UserDao(){
		
	}
	
	public UserDao( DataSource dataSource ){
		this.dataSource = dataSource;
	}
	
	public void setDataSource( DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	

	public void add( User user) throws ClassNotFoundException, SQLException{
		//connect to db
		//add user using stmt
		
		Connection c = null;
		PreparedStatement ps = null;
		
		c = dataSource.getConnection();
		
		ps = c.prepareStatement("insert into users(id, name, password) values(?,?,?)");
		
		ps.setString(1, user.getId());
		ps.setString(2, user.getName());
		ps.setString(3, user.getPassword());
		
		ps.executeUpdate();
		ps.close();
		c.close();
	}
	
	
	public User get(String id) throws ClassNotFoundException, SQLException{
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		c = dataSource.getConnection();
	
		ps = c.prepareStatement("select * from users where id=?");
		ps.setString(1, id);
		rs = ps.executeQuery();
		
		
		User user = null;
		if( rs.next()  ){
			user = new User();
			user.setId(rs.getString("id"));
			user.setName(rs.getString("name"));
			user.setPassword(rs.getString("password"));
		}
		
		if(user == null) throw new EmptyResultDataAccessException(1);
		
		rs.close();
		ps.close();
		c.close();
		
		return user;
	}
	
	public void deleteAll() throws SQLException{
		Connection c = null;
		PreparedStatement ps = null;
		
		try {
			c = dataSource.getConnection();
			ps = c.prepareStatement("delete from users");		
			ps.executeUpdate();
		} catch( SQLException e ){
			throw e;
		} finally{
			if( ps != null){
				try{
					ps.close();
				} catch( SQLException e){
					
				}
			}
			
			if( c != null){
				try{
					c.close();
				} catch( SQLException e){
					
				}
			}
		}
		
		
		ps.close();
		c.close();
	}
	
	public int getCount() throws SQLException{
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			c = dataSource.getConnection();
			ps = c.prepareStatement("select count(*) from users");
			
			
			rs = ps.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch ( SQLException e){
			throw e;
		} finally{
			if(rs != null){
				try{
					rs.close();
				} catch(SQLException e){
				}
			}
			if(ps != null){
				try{
					ps.close();
				} catch(SQLException e){
				}
			}
			if(c != null){
				try{
					c.close();
				} catch(SQLException e){
				}
			}
			
			rs.close();
			ps.close();
			c.close();
		}
	}
	
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub

		
	}

}
